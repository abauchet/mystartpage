import { Component, Inject, inject } from '@angular/core'
import { RouterLink } from '@angular/router'
import { CheckboxModule } from 'primeng/checkbox'
import { PasswordModule } from 'primeng/password'
import { FormsModule } from '@angular/forms'
import { ButtonModule } from 'primeng/button'
import { AppStore } from '../../app.store'
import { GqlService } from '../services/gql.service'

@Component({
  standalone: true,
  imports: [RouterLink, PasswordModule, CheckboxModule, FormsModule, ButtonModule],
  template: `
    <div
      class="surface-ground flex align-items-center justify-content-center min-h-screen min-w-screen overflow-hidden"
    >
      <div class="flex flex-column align-items-center justify-content-center">
        <div
          style="border-radius:56px; padding:0.3rem; background: linear-gradient(180deg, var(--primary-color) 10%, rgba(33, 150, 243, 0) 30%);"
        >
          <div class="w-full surface-card py-8 px-5 sm:px-8" style="border-radius:53px">
            <div class="text-center mb-5">
              <img src="vite.svg" alt="Image" height="50" class="mb-3" />
              <div class="text-900 text-3xl font-medium mb-3">Sign in</div>
            </div>

            <div>
              <label for="fullPath" class="block text-900 text-xl font-medium mb-2">Path</label>
              <input
                id="fullPath"
                [(ngModel)]="fullPath"
                type="text"
                placeholder="gitlab project path"
                pInputText
                class="w-full md:w-22rem mb-5"
                style="padding:1rem"
              />

              <label for="bearerToken" class="block text-900 font-medium text-xl mb-2">Token</label>
              <p-password
                id="bearerToken"
                [(ngModel)]="bearerToken"
                placeholder="Bearer Token"
                [toggleMask]="true"
                styleClass="mb-5"
                inputStyleClass="w-full p-3 md:w-22rem"
              ></p-password>

              <div class="flex align-items-center justify-content-between mb-5 gap-5">
                <div class="flex align-items-center">
                  <p-checkbox id="rememberme1" [binary]="true" styleClass="mr-2"></p-checkbox>
                  <label for="rememberme1">Remember me</label>
                </div>
              </div>
              <button pButton pRipple label="Sign In" class="w-full p-3 text-xl" (click)="login()"></button>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host ::ng-deep .pi-eye,
      :host ::ng-deep .pi-eye-slash {
        transform: scale(1.6);
        margin-right: 1rem;
        color: var(--primary-color) !important;
      }
    `,
  ],
})
export default class LoginComponent {
  private appStore = inject(AppStore)
  private gqlService = inject(GqlService)
  public fullPath = this.appStore.fullPath
  public bearerToken!: string
  public valCheck: string[] = ['remember']

  public login() {
    this.appStore.fullPath = this.fullPath
    this.appStore.bearerToken = this.bearerToken
    this.gqlService.getPages().subscribe(() => null)
  }
}
