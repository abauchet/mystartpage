import { Injectable, inject } from '@angular/core'
import { Router } from '@angular/router'

@Injectable()
export class AppStore {
  private router = inject(Router)
  private _fullPath = window.localStorage.getItem('FULL_PATH') ?? ''
  private _bearerToken = window.localStorage.getItem('BEARER_TOKEN') ?? ''

  public set fullPath(value: string) {
    this._fullPath = value
    window.localStorage.setItem('FULL_PATH', value)
  }

  public get fullPath(): string {
    return this._fullPath
  }

  public set bearerToken(value: string) {
    this._bearerToken = value
    window.localStorage.setItem('BEARER_TOKEN', value)
  }

  public get bearerToken(): string {
    return this._bearerToken
  }

  public get hasValidBearerToken(): boolean {
    return (window.localStorage.getItem('BEARER_TOKEN') ?? '') !== ''
  }

  public logout(): void {
    window.localStorage.removeItem('BEARER_TOKEN')
    this.router.navigate(['login'])
  }
}
