import { provideHttpClient, withFetch } from '@angular/common/http'
import { ApplicationConfig, importProvidersFrom, inject } from '@angular/core'
import { provideClientHydration } from '@angular/platform-browser'
import { provideFileRouter } from '@analogjs/router'

import { APOLLO_OPTIONS, Apollo } from 'apollo-angular'
import { HttpLink } from 'apollo-angular/http'
import { ApolloLink, InMemoryCache } from '@apollo/client/core'
import { setContext } from '@apollo/client/link/context'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { AppStore } from '../app.store'

const uri = 'https://gitlab.com/api/graphql'

export function createApollo(httpLink: HttpLink) {
  const basic = setContext(() => ({
    headers: {
      Accept: 'charset=utf-8',
    },
  }))
  console.log('createApollo', `Bearer ${window.localStorage.getItem('BEARER_TOKEN') || ''}`)
  const auth = setContext(() => ({
    headers: {
      Authorization: `Bearer ${window.localStorage.getItem('BEARER_TOKEN') || ''}`,
    },
  }))

  const link = ApolloLink.from([basic, auth, httpLink.create({ uri })])
  const cache = new InMemoryCache()

  return {
    link,
    cache,
  }
}

export const appConfig: ApplicationConfig = {
  providers: [
    AppStore,
    provideFileRouter(),
    provideHttpClient(withFetch()),
    provideClientHydration(),
    [importProvidersFrom([BrowserAnimationsModule])],
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink],
    },
    Apollo,
  ],
}
