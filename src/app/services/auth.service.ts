import { inject } from '@angular/core'
import { Router } from '@angular/router'
import { AppStore } from '../../app.store'

export function authGuard() {
  const router = inject(Router)
  const appStore = inject(AppStore)

  return !appStore.hasValidBearerToken ? router.navigate(['login']) : true
}
