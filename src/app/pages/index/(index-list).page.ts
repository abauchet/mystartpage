import { ChangeDetectionStrategy, Component, inject } from '@angular/core'
import { GqlService, WBookmark, WTodo } from '../../services/gql.service'
import { PanelModule } from 'primeng/panel'
import { CheckboxModule } from 'primeng/checkbox'
import { CardModule } from 'primeng/card'

@Component({
  selector: 'app-home',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CardModule, CheckboxModule],
  template: `
    <div class="grid">
      @for (widget of widgets(); track widget) {
      <p-card [header]="widget.title" class="col">
        <p>
          @for (data of widget.data; track data) { @if(isBookmark(data)){
          <a [href]="data.href" target="_blank"><img [src]="data.imgsrc" class="h-1rem" />{{ data.title }}</a>
          } @else {
          <p-checkbox name="groupname" [value]="data.value" [label]="data.title"></p-checkbox>
          } }
        </p>
      </p-card>
      } @empty { Empty list of widgets }
    </div>
  `,
  styles: [
    `
      .logo {
        will-change: filter;
      }
      .logo:hover {
        filter: drop-shadow(0 0 2em #646cffaa);
      }
    `,
  ],
})
export default class HomeComponent {
  private gqlService = inject(GqlService)
  public widgets = this.gqlService.getWidgets()

  public isBookmark(bookmark: WBookmark | WTodo): bookmark is WBookmark {
    return (bookmark as WBookmark).imgsrc !== undefined
  }
}
