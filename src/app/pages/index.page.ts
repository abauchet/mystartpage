import { ChangeDetectionStrategy, Component, Inject, inject } from '@angular/core'
import { RouterOutlet } from '@angular/router'
import TopbarComponent from '../components/topbar.component'
import { RouteMeta } from '@analogjs/router'
import { authGuard } from '../services/auth.service'

export const routeMeta: RouteMeta = {
  title: 'My Start Page',
  canActivate: [() => authGuard()],
}
@Component({
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [RouterOutlet, TopbarComponent],
  template: `
    <div class="layout-wrapper">
      <app-topbar></app-topbar>
      <div class="layout-main-container">
        <div class="layout-main">
          <router-outlet></router-outlet>
        </div>
      </div>
      <div class="layout-mask"></div>
    </div>
  `,
  styles: [
    `
      .layout-main-container {
        display: flex;
        flex-direction: column;
        min-height: 100vh;
        justify-content: space-between;
        padding: 7rem 2rem 2rem 4rem;
        transition: margin-left 0.2s;
      }

      .layout-main {
        flex: 1 1 auto;
      }
    `,
  ],
})
export default class HomeComponent {}
