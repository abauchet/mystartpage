import { Injectable, Signal, inject } from '@angular/core'
import { toSignal } from '@angular/core/rxjs-interop'
import { Apollo, gql } from 'apollo-angular'
import { catchError, map, tap } from 'rxjs'
import { AppStore } from '../../app.store'
import { Observable } from '@apollo/client'
import { Router } from '@angular/router'

type GqlPages = {
  project: {
    pages: {
      nodes: {
        id: string
        title: string
        description: string
      }[]
    }
  }
}

type GqlWidgets = {
  project: {
    widgets: {
      nodes: {
        title: string
        description: string
        labels: {
          nodes: {
            title: string
          }[]
        }
      }[]
    }
  }
}

type Page = {
  id: string
  title: string
  description: string
}

type Widget = {
  title: string
  data: WDataType[]
  type: WType
}

type WType = 'bookmarks' | 'todos' | 'notes'
type WDataType = WBookmark | WTodo

export type WBookmark = {
  title: string
  href: string
  imgsrc: string
}

export type WTodo = {
  title: string
  value: boolean
}

@Injectable({ providedIn: 'root' })
export class GqlService {
  private apollo = inject(Apollo)
  private appStore = inject(AppStore)
  private router = inject(Router)

  private pagesQuery = this.apollo.watchQuery<GqlPages>({
    query: gql`
      query ($fullPath: ID!) {
        project(fullPath: $fullPath) {
          pages: milestones {
            nodes {
              id
              title
              description
            }
          }
        }
      }
    `,
    variables: {
      fullPath: this.appStore.fullPath,
    },
  })

  private widgetsQuery = this.apollo.watchQuery<GqlWidgets>({
    query: gql`
      query ($fullPath: ID!) {
        project(fullPath: $fullPath) {
          widgets: issues {
            nodes {
              title
              description
              labels {
                nodes {
                  title
                }
              }
              upvotes
              milestone {
                id
                title
              }
            }
          }
        }
      }
    `,
    variables: {
      fullPath: this.appStore.fullPath,
    },
  })

  public getWidgets() {
    return toSignal(
      this.widgetsQuery.valueChanges.pipe(
        map(
          ({
            data: {
              project: { widgets },
            },
          }) =>
            widgets.nodes.map(
              m =>
                ({
                  title: m.title,
                  type: m.labels.nodes[0].title,
                  data: JSON.parse(m.description) as WDataType[],
                } as Widget)
            )
        ),
        catchError(err => {
          console.error('getWidgets', err)
          this.appStore.logout()
          return []
        })
      )
    )
  }

  public getPages() {
    return this.pagesQuery.valueChanges.pipe(
      map(
        ({
          data: {
            project: { pages },
          },
        }) =>
          pages.nodes.map(
            ({ id, title, description }) =>
              ({
                id,
                title,
                description,
              } as Page)
          )
      ),
      tap(() => {
        this.router.navigate([''])
      }),
      catchError(err => {
        console.error('getPages', err)
        this.appStore.logout()
        return []
      })
    )
  }

  public updateWidgets() {
    this.widgetsQuery.refetch()
  }
}
